Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: qqc2-breeze-style
Source: https://invent.kde.org/plasma/qqc2-breeze-style
Upstream-Contact: plasma-devel@kde.org

Files: *
Copyright: 2020, Noah Davis <noahadvs@gmail.com>
License: LGPL-2.1+3+KDEeV

Files: kirigami-plasmadesktop-integration/kirigamiplasmafactory.cpp
       kirigami-plasmadesktop-integration/kirigamiplasmafactory.h
       kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp
       kirigami-plasmadesktop-integration/plasmadesktoptheme.h
       style/impl/MobileCursor.qml
       style/impl/MobileTextActionsToolBar.qml
       style/kirigami/AbstractApplicationHeader.qml
       style/kirigami/AbstractListItem.qml
       style/kirigami/ApplicationWindow.qml
       style/kirigami/SwipeListItem.qml
       style/kirigami/Theme.qml
       style/kirigami/Units.qml
Copyright: 2015-2018, Marco Martin <mart@kde.org>
           2016, Marco Martin <notmart@gmail.com>
           2020, Noah Davis <noahadvs@gmail.com>
License: LGPL-2+

Files: style/impl/iconlabellayout.cpp
       style/impl/iconlabellayout.h
       style/impl/iconlabellayout_p.h
       style/impl/qquickicon.cpp
       style/impl/qquickicon_p.h
       style/qtquickcontrols/Drawer.qml
       style/qtquickcontrols/HorizontalHeaderView.qml
       style/qtquickcontrols/MenuBarItem.qml
       style/qtquickcontrols/MenuItem.qml
       style/qtquickcontrols/Menu.qml
       style/qtquickcontrols/Popup.qml
       style/qtquickcontrols/RoundButton.qml
       style/qtquickcontrols/SplitView.qml
       style/qtquickcontrols/VerticalHeaderView.qml
Copyright: 2017-2018, Marco Martin <mart@kde.org>
           2020, Noah Davis <noahadvs@gmail.com>
           2017-2020, The Qt Company Ltd.
License: LGPL-3_or_GPL-2+

Files: style/impl/SliderGroove.qml
       style/qqc2breezestyleplugin.cpp
       style/qqc2breezestyleplugin.h
       style/qtquickcontrols/RangeSlider.qml
Copyright: 2017, Marco Martin <mart@kde.org>
           2020, Noah Davis <noahadvs@gmail.com>
           2017, The Qt Company Ltd.
License: LGPL-3+KDEeV_or_GPL-2++KDEeV

Files: debian/*
Copyright: 2021, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2021, Norbert Preining <norbert@preining.info>
License: LGPL-2.1+3+KDEeV

License: LGPL-2+
 SPDX-License-Identifier: LGPL-2.0-or-later
 --
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.1+3+KDEeV
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) version 3, or any
 later version accepted by the membership of KDE e.V. (or its
 successor approved by the membership of KDE e.V.), which shall
 act as a proxy defined in Section 6 of version 3 of the license.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 --
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1’,
 likewise, the complete text of the GNU Lesser General Public License version
 3 can be found in `/usr/share/common-licenses/LGPL-3’.

License: LGPL-3+KDEeV_or_GPL-2++KDEeV
 LGPL-3.0-only OR GPL-2.0-or-later OR LicenseRef-KDE-Accepted-LGPL OR LicenseRef-KFQF-Accepted-GPL
 --
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the license or (at your option) any later version
 that is accepted by the membership of KDE e.V. (or its successor
 approved by the membership of KDE e.V.), which shall act as a
 proxy as defined in Section 6 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 Alternatively, this file may be used under the terms of the GNU
 General Public License version 2.0 or (at your option) the GNU General
 Public license version 3 or any later version approved by the KDE Free
 Qt Foundation. The licenses are as published by the Free Software
 Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
 included in the packaging of this file. Please review the following
 information to ensure the GNU General Public License requirements will
 be met: https://www.gnu.org/licenses/gpl-2.0.html and
 https://www.gnu.org/licenses/gpl-3.0.html.
 --
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 3 can be found in `/usr/share/common-licenses/LGPL-3’,
 likewise, the complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2’.

License: LGPL-3_or_GPL-2+
 LGPL-3.0-only OR GPL-2.0-or-later
 --
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 3 can be found in `/usr/share/common-licenses/LGPL-3’,
 likewise, the complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2’.
